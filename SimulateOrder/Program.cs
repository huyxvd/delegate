﻿Console.OutputEncoding = System.Text.Encoding.UTF8;


Order order = new Order();

order.ProcessPayment(1);

class Order
{
    public void ProcessPayment(int orderId)
    {
        Console.WriteLine($"Thanh toán order {orderId}");

        // Gửi sms báo đã được thanh toán
        SentSMS sentSMS = new SentSMS();
        sentSMS.SentSMSNotification(orderId);

        // Gửi email thông báo về đơn hàng
        SentEmail sentEmail = new SentEmail();
        sentEmail.SentEmailNotification(orderId);

        // Gửi thông tin cho đơn vị shipping
        ScheduleShippingOrder scheduleShippingOrder = new ScheduleShippingOrder();
        scheduleShippingOrder.ScheduleShipping(orderId);
    }
}

class SentSMS
{
    public void SentSMSNotification(int orderId)
    {
        Console.WriteLine($"thông báo qua SMS {orderId} đã được thanh toán");
    }
}

class SentEmail
{
    public void SentEmailNotification(int orderId)
    {
        Console.WriteLine($"thông báo qua email {orderId} đã được thanh toán");
    }
}

class ScheduleShippingOrder
{
    public void ScheduleShipping(int orderId)
    {
        Console.WriteLine($"Gửi thông tin đơn hàng {orderId} đến đơn vị shipping");
    }
}