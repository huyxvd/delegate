﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

SentSMS sentSMS = new SentSMS();
SentEmail sentEmail = new SentEmail();
ScheduleShippingOrder scheduleShippingOrder = new ScheduleShippingOrder();

Order order = new Order();

order.OnPaymentProcessed += sentSMS.SentSMSNotification; // Gửi sms báo đã được thanh toán

order.OnPaymentProcessed += sentEmail.SentEmailNotification; // Gửi email thông báo về đơn hàng

order.OnPaymentProcessed += scheduleShippingOrder.ScheduleShipping; // Gửi thông tin cho đơn vị shipping

order.ProcessPayment(1); // gọi payment

class Order
{
    public event PaymentAction OnPaymentProcessed;
    public void ProcessPayment(int orderId)
    {
        Console.WriteLine($"Thanh toán order {orderId}");
        OnPaymentProcessed?.Invoke(orderId);// kiểm tra null trước khi thực thi
    }
}

class SentSMS
{
    public void SentSMSNotification(int orderId)
    {
        Console.WriteLine($"thông báo qua SMS {orderId} đã được thanh toán");
    }
}

class SentEmail
{
    public void SentEmailNotification(int orderId)
    {
        Console.WriteLine($"thông báo qua email {orderId} đã được thanh toán");
    }
}

class ScheduleShippingOrder
{
    public void ScheduleShipping(int orderId)
    {
        Console.WriteLine($"Gửi thông tin đơn hàng {orderId} đến đơn vị shipping");
    }
}

public delegate void PaymentAction(int orderId);